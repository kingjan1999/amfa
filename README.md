# AMFA

Die Abimottofindungsapplikation soll eine Konsens-Bildung für ein Abimotto erleichtern.
Dazu stellt es ein Webinterface bereit, dessen technischer Hintergrund im Folgenden dargstellt wird:

- Server:
  - Meteor
  - MongoDB
  - Publications über DDP

- Client:
  - Meteor
  - Materialize
  - Subscriptions über DDP
  - Minimongo
