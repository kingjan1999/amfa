Meteor.methods
  voteFor: (id) ->
    check id, String
    if(@userId)
      fid = Meteor.user().services.facebook.id
      if Votes.find({motto: id, voter: fid}).count() > 0
        throw new Meteor.Error 'already-voted'
      Votes.insert
        motto: id
        voter: fid
    else
      throw new Meteor.Error 'not-authorized'
  devoteFor: (id) ->
    check id, String
    if(@userId)
      fid = Meteor.user().services.facebook.id
      if Votes.find({motto: id, voter: fid}).count() < 1
        throw new Meteor.Error 'not-voted'
      Votes.remove
        motto: id
        voter: fid
    else
      throw new Meteor.Error 'not-authorized'
  top: (limit) ->
    check limit, Match.Integer
    Votes.aggregate { $group:
      _id: '$motto'
      total: $sum: 1
    }, {$sort: {total: -1}}, {$limit: limit}
