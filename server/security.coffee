Security.defineMethod 'ifIsJan',
  fetch: [],
  transform: null
  deny: (type, arg, userId, doc) ->
    user = Meteor.users.findOne({_id: userId})
    return user.services.facebook.id == 1716278488601289

Mottos.permit(['insert']).ifLoggedIn().apply()

Mottos.permit(['remove', 'update']).ifIsJan().apply()

Votes.permit(['insert']).ifLoggedIn().apply()

Votes.permit(['remove', 'update']).ifIsJan().apply()
