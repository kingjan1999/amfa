Meteor.publish 'Mottos', -> Mottos.find()
Meteor.publish "Motto", (motto) ->
  check motto String
  Motto.find({_id: motto})

buildRegExp = (searchText) ->
  # this is a dumb implementation
  parts = searchText.trim().split(/[ \-\:]+/)
  new RegExp('(' + parts.join('|') + ')', 'ig')

SearchSource.defineSource 'Mottos', (searchText, options) ->
  `var options`
  options =
    limit: 100
  if searchText
    regExp = buildRegExp(searchText)
    selector = spruch : regExp
    Mottos.find(selector, options).fetch()
  else
    Mottos.find({}, options).fetch()
