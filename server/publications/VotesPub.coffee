Meteor.publishComposite 'Votes',
  find: ->
    Votes.find({})
  children: [
    {
      find: (vote) ->
        Mottos.find {_id: vote.motto},
          limit: 1
    }
  ]
