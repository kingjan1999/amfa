configureFacebook = (config) ->
  # first, remove configuration entry in case service is already configured
  ServiceConfiguration.configurations.remove service: 'facebook'
  ServiceConfiguration.configurations.insert
    service: 'facebook'
    appId: config.clientId
    secret: config.secret
  return

# set the settings object with meteor --settings private/settings-local.json

Meteor.startup ->
  facebookConfig = Meteor.settings.facebook
  if facebookConfig
    console.log 'Got settings for facebook', facebookConfig
    configureFacebook facebookConfig

Accounts.validateNewUser (user) ->
  at = user.services.facebook.accessToken
  id = user.services.facebook.id
  responseContent = HTTP.get('https://graph.facebook.com/v2.5/1583564048582481/members?limit=200', params: access_token: at)

  if !responseContent.data or !responseContent.data.data
    throw new (Meteor.Error)('No valid JSON for group membership validation')
  group_members = responseContent.data.data
  ids = lodash.map(group_members, 'id')

  if !lodash.includes(ids, id)
    throw new (Meteor.Error)('Not in group "Abitur 2017"!')
  return true
