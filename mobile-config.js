App.info({
id: 'me.kingjan1999.amfa',
name: 'AMFA',
description: 'Abi-Motto-Findungs-Applikation',
author: 'Jan Beckmann'
});

App.icons({
'iphone': 'resources/icons/iphone-60.png',
'iphone_2x': 'resources/icons/iphone-60@2x.png',
'iphone_3x': 'resources/icons/iphone-60@3x.png',
'ipad': 'resources/icons/ipad_1.png',
'ipad_2x': 'resources/icons/ipad@2x.png',

'android_mdpi': 'resources/icons/android_mdpi.png',
'android_hdpi': 'resources/icons/android_hdpi.png',
'android_xhdpi': 'resources/icons/android_xhdpi.png'
});

App.setPreference('HideKeyboardFormAccessoryBar', true);

App.accessRule("https://fonts.googleapis.com/");
App.accessRule("https://cdnjs.cloudflare.com/")
