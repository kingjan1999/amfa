Template['stats'].helpers(
)

Template['stats'].events(
)

buildMottoBar = ->
  return new Highcharts.Chart
    chart:
      type: 'bar'
      renderTo: 'mottoChart'
    title:
      text: 'Top Abimottos'
    xAxis:
      categories: ['Motto']
    yAxis:
      title:
        text: 'Anzahl der Stimmen'
    series: [
    ]


Template.stats.rendered = ->
  chart = buildMottoBar()
  @autorun ->
    Votes.find({}).fetch()
    updateTopMottos()
    i = 0
    mottos = Session.get("topMottos") ? Session.get("topMottos") || []
    while chart.series.length > 0 # Clear chart
      chart.series[0].remove false
    while i < mottos.length # Add new
      chart.addSeries  mottos[i], false
      i++
    chart.redraw()

    return undefined

updateTopMottos = ->
  Meteor.call "top", 10, (error, result) ->
    if error
      console.log "error", error
    if result
      to_add = []
      result.forEach (motto) ->
        mot = Mottos.findOne(motto._id)
        to_add.push
          name: mot.spruch
          data: [motto.total]
        return undefined
      Session.set("topMottos", to_add)
      return undefined
