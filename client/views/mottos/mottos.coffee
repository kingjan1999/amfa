Template['mottol'].helpers
  'canDelete': ->
    return Meteor.user()?.services?.facebook?.id == '1716278488601289'

Template['mottos'].helpers
  'mottosAnzahl': ->
    return Mottos.find({}).count()

Template['voteBtn'].events
  'click #voteBtn': ->
    Meteor.call 'voteFor', @_id, (error) ->
      return undefined unless error?
      console.log error
      if error.error == 'already-voted'
        Materialize.toast('Du hast bereits für dieses Motto abgestimmt!', 2000)
      else if error.error == 'not-authorized'
        Materialize.toast 'Du bist nicht autorisiert, für dieses Motto abzustimmen', 2000
      else
        Materialize.toast 'Ein unbekannter Fehler ist aufgetreten', 5000
  'click #devoteBtn': ->
    Meteor.call 'devoteFor', @_id, (error) ->
      return undefined unless error?
      console.log error
      if error.error == 'not-voted'
        Materialize.toast 'Du hast nicht für dieses Motto abgestimmt', 2000
      else if error.error == 'not-authorized'
        Materialize.toast 'Du bist nicht autorisiert, für dieses Motto abzustimmen', 2000
      else
        Materialize.toast 'Ein unbekannter Fehler ist aufgetreten', 5000

Template['voteBtn'].helpers
  'disabled': ->
    return false unless Votes.find({motto: @_id, voter: Meteor.user().services.facebook.id}).count() > 0
    true
  'notdisabled': ->
    return true unless Votes.find({motto: @_id, voter: Meteor.user().services.facebook.id}).count() > 0
    false


AutoForm.hooks
  insertMottoForm:
    onSuccess: (formType, result) ->
      Materialize.toast("Du hast dieses Motto erfolgreich vorgeschlagen! (<a href=/mottos/#{result}/>Ansehen</a>)", 3000)


options =
  keepHistory: 1000 * 60 * 5
  localSearch: true

fields = [
    'spruch'
  ]

MottosSearch = new SearchSource('Mottos', fields, options)

Template['searchResult'].helpers
  'getMottos': ->
    MottosSearch.getData()
  'isLoading': ->
    MottosSearch.getStatus().loading

Template['searchResult'].rendered = ->
  MottosSearch.search ''
  return undefined

Template.searchBox.events 'keyup #search-box': _.throttle(((e) ->
  text = $(e.target).val().trim()
  MottosSearch.search text
  return
), 200)
