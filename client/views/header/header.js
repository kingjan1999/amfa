Template.header.created = function () {
  Session.set('isActive', false);
  Session.set('showLogin', false);
};


Template['header'].rendered = function() {
  $('.button-collapse').sideNav({
      menuWidth: 240, // Default is 240
      edge: 'left', // Choose the horizontal origin
      closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
    }
  );
};

Template['header'].events({
  'click .log-out-button' : function () {
    console.log("Klick");
    Meteor.logout();
  }
});
