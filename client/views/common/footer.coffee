Template.footer.helpers
  top5: ->
    Session.get('top5')
  findMotto: (motto) ->
    Mottos.findOne(_id: motto)
  stimme: (zahl) ->
    if zahl > 1
      "#{zahl} Stimmen"
    "#{zahl} Stimme"
  index: (zahl) ->
    zahl + 1

updateTop5 = ->
  Meteor.call "top", 5 , (error, result) ->
    if error
      console.log "error", error
    if result
      Session.set('top5', result)
  return undefined


Template.footer.onCreated ->
  self = @
#  @top5 = new ReactiveVar ['Warten', 'auf', 'Antwort', 'des' , 'Servers']
  self.subscribe 'Votes', ->
    Tracker.autorun ->
      votes = Votes.find({}).fetch()
      updateTop5()
