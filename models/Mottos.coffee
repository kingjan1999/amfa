Schemas = {} unless Schemas?

@Mottos = new Meteor.Collection "Mottos"

Schemas.Mottos = new SimpleSchema(
  spruch:
    type: String
    label: 'Spruch'
    max: 200
  author:
    type: String
    label: 'Urheber'
    autoValue: ->
      ano = this.field('anon').value
      if ano
        'Anonym'
      else
        Meteor.user().profile.name
  anon:
    type: Boolean
    label: 'Anonym'
    autoform:
      type: "switch"
      trueLabel: "Anonym bleiben"
      falseLabel: "Urheber veröffentlichen"
  aid:
    type: String
    label: 'ID des Urhebers'
    regEx: SimpleSchema.RegEx.Id
    autoValue: ->
      Meteor.userId()
  jux_kompatibel:
    type: Boolean
    label: "Abi-Jux-Tag-Kompatibilität"
    autoform:
      type: "switch"
      trueLabel: "Inkludiert Abi-Jux-Tag-Motto"
      falseLabel: "Separates Abi-Jux-Tag-Motto erforderlich"
  abiball_kompatibel:
    type: Boolean
    label: "Abiball-Thema-Kompatibilität"
    autoform:
      type: "switch"
      trueLabel: "Inkludiert Abiball-Thema"
      falseLabel: "Separates Abiball-Thema erforderlich"
)

Mottos.attachSchema(Schemas.Mottos);

Mottos.helpers
  votes: ->
    Votes.find({motto: this._id})
  votesCount: ->
    Votes.find({motto: this._id}).count()
  rank: ->
    count = Votes.find({motto: this._id}).count()
    rank = 1
    cursor = Mottos.find({_id: {$ne: this._id}});
    cursor.forEach (row) -> #Eiine Performance zum Kotzen. Datenbankseitige Lösung sollte - TODO - unbedingt angestrebt werden, scheint aber kaum lösbar
      t_count = Votes.find({motto: row._id}).count();
      rank++ if t_count > count
      return undefined
    rank
