Schemas = {} unless Schemas?

@Votes = new Meteor.Collection "Votes"

Schemas.Votes = new SimpleSchema(
  voter:
    type: String
    label: 'Wähler ID'
  motto:
    type: String
    label: "Motto ID"
    regEx: SimpleSchema.RegEx.Id
  createdAt:
    type: Date
    label: "Erstellungsdatum"
    autoValue: ->
      if @isInsert
        return new Date
      else if @isUpsert
        return { $setOnInsert: new Date }
      else
        @unsert()
      return
)

Votes.attachSchema(Schemas.Votes);

Votes.helpers
  motto: ->
    Mottos.findOne(this.motto)
