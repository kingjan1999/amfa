Router.route '/mottos/',
  template: 'mottos'
  waitOn: ->
    Meteor.subscribe('Mottos')
    Meteor.subscribe "userData"
  data: ->
    {mottos: Mottos.find({}).fetch() }
  loadingTemplate: 'loading'

Router.route '/mottos/add', ->
  this.render 'addMotto'

Router.route '/mottos/:_id',
  template: 'motto'
  waitOn: ->
    Meteor.subscribe('Mottos')
    Meteor.subscribe "userData"
    Meteor.subscribe "Votes"
  data: ->
    Mottos.findOne({_id: this.params._id})
  loadingTemplate: 'loading'
