Router.route '/', ->
  this.render 'home'
  SEO.set {title: 'Start - AMFA'}

Router.route '/stats',
  template: 'stats'
  waitOn: ->
    Meteor.subscribe('Mottos')
    Meteor.subscribe "userData"
    Meteor.subscribe "Votes"
  loadingTemplate: 'loading'

AccountsTemplates.configureRoute 'signIn',
  name: 'signin'
  path: '/login'
  template: 'login'
  redirect: '/mottos'
